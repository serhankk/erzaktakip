
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<head>
	<link rel="icon" href="icons/table-solid.png" type="image/x-icon" />
	<?php
	$baglanti = new mysqli("localhost", "serhan", "test","erzakVeritabani");

	if ($baglanti->connect_errno > 0){
		die("<b>Bağlantı hatası:</b>" . $baglanti->connect_error);
	}

	$baglanti->set_charset("utf8");
	$sorgu = $baglanti->query("SELECT * FROM evdekilerTablosu");
	if ($baglanti->errno > 0) {
	die("Sorgu Hatasi! " . $baglanti->error);
	}
	?>
<title>Gümrük | Ürünleri Göster</title>
<style>

body {
  margin: 1.5em;
}

table{
	color: #0c1931;
	clear: both;
	border-collapse: collapse;
	border-radius: 1em;
	overflow: hidden;
	width: auto;
}

th{
	padding: 1em;
	font-family: monospace;
	font-size: 16px;
	color: #0c1931;
  border: 4px solid #0c1931;
}

td{
	font-family: monospace;
	font-size: 16px;
	color: #0c1931;
  border: 4px solid #0c1931;
	text-align: center;
}
</style>
</head>
<body bgcolor="#ffecdf">
<table bgcolor="#b1ccff">
	<tr>
    <th>Ürün Adı</th>
    <th>Ürün Cinsi</th>
    <th>Ürün Fiyatı</th>
    <th>Ürün Kilosu</th>
    <th>Ürün Adedi</th>
    <th>Ürün Kategorisi</th>
    <th>Ödeme Şekli</th>
		<th>Alışveriş Tarihi</th>
  </tr>

<?php while($cikti = $sorgu->fetch_assoc()){
 echo "<tr>";
 echo "<td>" . $cikti['UrunAdi'] . "</td>";
 echo "<td>" . $cikti['UrunCinsi'] . "</td>";
 echo "<td>" . $cikti['UrunFiyati'] . "</td>";
 echo "<td>" . $cikti['UrunKilosu'] . "</td>";
 echo "<td>" . $cikti['UrunAdedi'] . "</td>";
 echo "<td>" . $cikti['UrunKategorisi'] . "</td>";
 echo "<td>" . $cikti['OdemeSekli'] . "</td>";
 echo "<td>" . $cikti['AlisverisTarihi'] . "</td>";
 echo"</tr>";
}
$sorgu->close();
$baglanti->close();
?>

</table>
</body>
</html>
